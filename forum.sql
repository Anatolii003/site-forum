-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 20 2019 г., 14:33
-- Версия сервера: 8.0.12
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `forum`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `text` text NOT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `topic_id`, `date`, `text`, `author`, `email`, `parent_id`) VALUES
(665, 156, '2019-03-23 03:11:54', 'Potřeboval bych nějak ošetřit chyby kde dojde k nějakému nepřevídanému fatal erroru tak aby kod dokázal pokračovat dál. Modelově mám cyklus:\n\nfor($i=0;$i<3­2;$i++)\n{\nfunkce1();\nfunkce1();//tady vznikne chyba, někde v půlce cyklu třeba.\nfunkce1();\n\n}\n\nJde mi o to že chyba co může nastat, je taková, co nečekám že vůbec může nastat. Jak takovou chybu detekovat?\n\nZkusil jsem: Zkoušel jsem na to jít přes vyjímky, ale bud jsem je nepochopil, nebo jsou na nic, pokud vyjímky musím sám odpalovat nějakou svou detekcí chyb a následným voláním throw. To pak jsem u problému že musím vědět co se může stát. Tudíž stejně musím neustále všude ifovat a kontrolovat vše.\n\nChci docílit: Jak zařídím aby se script s chybou vypořádal a pokračoval v dalším cyklu?', 'maxijoey', 'maxijoey@test.com', 0),
(666, 156, '2019-03-23 03:12:41', 'Záleží co myslíš tou chybou?\n\nMožná:\n\nfor($i=0;$i<32;$i++)\n{\n funkce1();\n try{\n  funkce1();//tady vznikne chyba, někde v půlce cyklu třeba.\n }catch(\\Exception $exception){\n  // V tento okamžik nastala nějaká chyba\n  // Můžeš něco vykonat nebo taky nic a program bude pokračovat na volání další funkce\n }\n funkce1();\n}\nSpíš jsem dej konkrétní kód, než tenhle pseudo - pokud ti nejde pouze o terorii.', 'nekukej', 'nekukej@test.com', 665),
(669, 161, '2019-03-23 03:19:38', 'Nejde mi aktivovat rozšíření Sodium (pro šifrování dat).\n\nmám:\nXAMPP 7.3.1\nPHP 7.3.1\nOS Windows 10\n\nZkusil jsem: v php.ini jsem odkomentoval řádek:\n\nextension=sodium\na vypnul jsem a znovu zapnul Apache přez XAMPP Control Panel (ten spouštím jako správce)\n\nPro editaci jsem použil Notepad++.\n\nPři zavolání:\n\nprint_r(get_loaded_extensions());\ntam Sodium není a samozřejmě nefunguje žádná funkce, např:\n\nrandom_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES);\nV xampp/php/ext/php_so­dium.dll je.\n\nChci docílit: Zprovoznit rozšíření Sodium.', 'Navry', 'navry@navry.com', 0),
(667, 156, '2019-03-23 03:14:26', 'Zalezi na tom, jak to chces mit resene.\n\nPokud chces resit vnejsi funkce jako celek (takovy priklad tam zrovna neni) nebo po jedne (p\n7 - ohcc at 163 dot com) a nebo jeste neco uvnitr (Example #3).\n\nhttp://php.net/manual/en/language.exceptions.php\n<?php\nfunction inverse($x) {\n    if (!$x) {\n        throw new Exception(\'Division by zero.\');\n    }\n    return 1/$x;\n}\n\ntry {\n    echo inverse(5) . \"\\n\";\n    echo inverse(0) . \"\\n\";\n} catch (Exception $e) {\n    echo \'Caught exception: \',  $e->getMessage(), \"\\n\";\n}\n\n// Continue execution\necho \"Hello World\\n\";\n?>\nV tomto examplu, kdyz nastane chyba, tak vyhodi exception, ten if. A nasledne ti to exception zachyti dalsi try To if tam ale nemusi byt. Jenze, pak ti vyhodi error return 1/$x; To bys pak musel dat do try.\n\nfor (...)\ntry {\n    $x = 1/$i;\n} catch (Exception $e) {\n    break; // kdyz nastane chyba, ukonci cyklus\n}\n---\n\nHele, ale ify bys prave resit mel. Vsechny situace bys mel predvidat. V opacnem pripade bych takovy program nechtel zaplatit, smazal a poslal te jinam.\n\nA jeste muzes zachytavat vsechny exception\nfunction exceptions_error_handler($severity, $message, $filename, $lineno) {\n    throw new ErrorException($message, 0, $severity, $filename, $lineno);\n}\n\nset_error_handler(\'exceptions_error_handler\');\n?>\nA nebo muzes chyby potlacovat, v tom pripade by ale cyklus pokracoval dal a neprerusil se \'break\'-em. To by melo smysl treba v pripade session nebo include. Ale pak ti take nemusi session spravne fungovat.\n@$x = 1/$i;\n// @session_start();\n// @include \'file.php\';', 'Peter Mlich', 'peter@test.com', 665),
(668, 156, '2019-03-23 03:15:06', 'Nekoukej, konkrétního kodu je hromada, proto ten pseudokod pro pochopeni. A jde mi hlavně o fatal errory a to jsem psal V tvem příkladu, to jsem právě zkoušel co píšeš, při fatal erroru to nefunguje script končí. Nedonutím ho pokračovat.\n\nPeter Milch Zavináče používám na jednom možná dvou míctech, jedno je autoloader druhé už nevím :-) Ten příklad s error handlerem nebo podobný na ten jsem narazil a je fakt ze takto:\n\nfunction shutdown_error() {\n$error = error_get_last();\nif ($error[\'type\'] & (E_ERROR | E_CORE_ERROR | E_COMPILE_ERROR | E_PARSE)) {\necho(\"Nastala chyba\");\n}\n}\nregister_shut­down_function(\'shut­down_error\');\n\nto umi fatal error detekovat. Ale nedaří se mi ho přesvědčit aby v cyklu pokračoval. Jinak ao ifuju kde můžu, ale je toho moc a občas se stane že fakt něco nepředvídáš a měl jsem právě za to, že na to jsou vyjímky. V takovým C# to tuším tak funguje. Co si pamatuju. Nebo to tam je prošpikovaný odpalováním vyjímek už v v základu dotnetu nevim. Jen vím že tam mi to pomáhalo, tady si to ošetřit spíš přidělá práci.. Nevidím proto moc velký rozdíl mezi ifama a vyjímkama.\n\nJediné co mě napadá je ukládat si číslo pruchodu cyklem nekam do souboru nebo db. Pokud detekuju fatalerror tak script naikludovat znovu s tím že se podívá kde skočil a nastaví začátek na další cyklus. Ale nevím aby to nebyla cesta někam do prdele.', 'maxijoey', 'maxijoey@test.com', 666),
(676, 161, '2019-04-15 21:01:40', 'ывафыва', 'fdvdfv', 'test@gmail.com', 669),
(677, 161, '2019-04-15 21:02:00', 'выафыва', 'fvdvfdv', 'test@gmail.com', 676);

-- --------------------------------------------------------

--
-- Структура таблицы `topics`
--

CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `name` tinytext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `topics`
--

INSERT INTO `topics` (`id`, `date`, `name`) VALUES
(156, '2019-03-23 03:11:22', 'Ošetření chyb v PHP'),
(157, '2019-03-23 03:17:44', 'Php kalendář - vytvoření a zachování více událostí'),
(158, '2019-03-23 03:18:17', 'generování sitemap a mazání'),
(159, '2019-03-23 03:18:35', 'Po přihlášení přesměrovat na požadovanou stránku'),
(160, '2019-03-23 03:18:49', 'odkaz na určitou část stránky'),
(161, '2019-03-23 03:19:07', 'Pomoc s aktivací rozšíření Sodium'),
(219, '2019-04-19 13:55:04', '111'),
(220, '2019-04-19 13:55:09', '222'),
(221, '2019-04-19 13:55:25', '111'),
(227, '2019-04-20 13:25:07', 'выаыва'),
(228, '2019-04-20 13:25:40', 'ываыа'),
(229, '2019-04-20 13:27:07', 'паавп'),
(240, '2019-04-20 13:47:26', '111'),
(241, '2019-04-20 13:49:16', '222'),
(242, '2019-04-20 13:49:50', 'qqq'),
(243, '2019-04-20 13:50:41', 'www'),
(244, '2019-04-20 13:51:14', '222'),
(245, '2019-04-20 13:54:55', '1'),
(246, '2019-04-20 14:28:55', '333'),
(247, '2019-04-20 14:29:48', '444');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `login` varchar(191) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `admin` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `password`, `admin`) VALUES
(5, 'test', 'test@gmail.com', '$2y$10$KpuYXRIoPvTPx0hO9Duzg.0F./n0zSrzZ1GrRnIwjAEkgih2hyCpi', NULL),
(13, 'admin', 'admin@gmail.com', '$2y$10$bZhE4OsOKGIAQLCmKkU99OadK/p8L6KG5Bunme.2wY8nncFMBeEHa', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=678;

--
-- AUTO_INCREMENT для таблицы `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
