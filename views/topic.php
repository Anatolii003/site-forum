<?php require_once ROOT . '/views/layouts/header.php' ?>

    <!--section of the current topic-->
    <section class="section bgg">
        <div class="container">
            <div class="title-area">
                <h2><?php echo $topic['name'] ?></h2>
            </div><!-- /.pull-right -->
        </div><!-- end container -->
    </section>
    <!--end section of the current topic-->


    <!--show comments-->
<?php
$arr = array();
$arr = Comment::getParentComment();
foreach ($arr as $row) {
    TreeComment::tree($row);
}
?>
    <!--end show comments-->


    <!--script delete comment-->
    <script type="text/javascript">
        $(document).ready(function () {
            $('a.bbp-forum-delete-topic').click(function (e) {
                e.preventDefault();
                var parent = $(this).parent();
                var id = $(this).attr("id");
                let isConfirmed = confirm('Are you sure you want to delete this comment?');
                if (isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: '?action=delete-comment',
                        data: {id: id},
                        beforeSend: function () {
                            parent.animate({'backgroundColor': '#fb6c6c'}, 300);
                        },
                        success: function () {
                            parent.slideUp(300, function () {
                                parent.remove();
                            });
                        }
                    });
                }
            });
        });
    </script>
    <!--end script delete comment-->


    <!--script create child comment-->
    <script type="text/javascript">
        $(document).ready(function () {
            $("a.reply").one('click', function () {
                var id = $(this).attr("id");
                var topic_id = $(this).attr("topic_id");
                $(this).parent().append("<div id='newform'><p><input type='text' name='author' id='author' placeholder='Your Name'/></p><p><input type='email' name='email' id='email' placeholder='Your Email'/></p><p><textarea name='text' id='text'></textarea></p><input type='hidden' name='parent_id' id='parent_id' value='" + id + "'/><input type='hidden' name='topic_id' id='topic_id' value='" + topic_id + "'/><p><button>Send</button></p></div><div id='error-child-message'></div>");
                $(function () {
                    $('textarea').autoResize();
                });
                $("button").click(function () {
                    var errors = '';
                    var pattern = /\S+@\S+\.\S+/;
                    var author = $("#author").val();
                    var text = $("#text").val();
                    var email = $("#email").val();
                    var parent_id = $("#parent_id").val();
                    var topic_id = $("#topic_id").val();
                    var date = $("#date").val();
                    if (author.length < 3) {
                        errors += '<p>Name must be longer than 3 characters</p>';
                    }
                    if (!pattern.test(email)) {
                        errors += '<p>Incorrect email</p>';
                    }

                    if (text.length < 5) {
                        errors += '<p>Message must be longer than 5 characters</p>';
                    }
                    if (errors == '') {
                        $.ajax({
                            url: "?action=add-comment",
                            type: "POST",
                            data: {
                                author: author,
                                text: text,
                                email: email,
                                parent_id: parent_id,
                                topic_id: topic_id,
                                date: date
                            },
                            success: function () {
                                $("#newform").prev().append("<div class='comment'><div class='container sitecontainer single-wrapper bgw'><strong>Your comment</strong><div class='authorbox'><div class='text'>" + text + "</div></div></div></div>");
                                $("#newform").remove();
                                $("#error-child-message").remove();
                            }
                        });
                    } else {
                        $('#error-child-message').html(errors);
                        $('#error-child-message').show();
                    }
                });
            });

        });
    </script>
    <!--end script create child comment-->



    <!--create comment-->
    <div id="new-comment"></div>
    <div class="container sitecontainer single-wrapper bgw">
        <div class="authorbox">

            <div id="error-message"></div>
            <div class="form">
                <p>
                    <input type="text" name="author" id="author" placeholder="Your Name"/>
                </p>
                <p>
                    <input type="email" name="email" id="email" placeholder="Your Email"/>
                </p>
                <p>
                    <textarea name="text" id="text"></textarea>
                </p>
                <input type="hidden" name="parent_id" id="parent_id" value="0"/>
                <input type="hidden" name="topic_id" id="topic_id" value="<?= $topicId ?>"/>
                <input type="hidden" name="date" id="date" value="<?= $date = date("Y-m-d H:i:s"); ?>"/>
                <p>
                    <button id="button">Send</button>
                </p>
            </div>

            <script type="text/javascript">
                $(document).ready(function () {
                    $("button#button").click(function () {
                        var errors = '';
                        var pattern = /\S+@\S+\.\S+/;
                        var author = $("#author").val();
                        var text = $("#text").val();
                        var email = $("#email").val();
                        var topic_id = $("#topic_id").val();
                        var parent_id = $("#parent_id").val();
                        var date = $("#date").val();
                        if (author.length < 3) {
                            errors += '<p>Name must be longer than 3 characters</p>';
                        }
                        if (!pattern.test(email)) {
                            errors += '<p>Incorrect email</p>';
                        }

                        if (text.length < 5) {
                            errors += '<p>Message must be longer than 5 characters</p>';
                        }
                        if (errors == '') {
                            $.ajax({
                                url: "?action=addComment",
                                type: "POST",
                                data: {
                                    author: author,
                                    text: text,
                                    email: email,
                                    parent_id: parent_id,
                                    topic_id: topic_id,
                                    date: date
                                },
                                success: function () {
                                    $("#new-comment").append("<div class='container sitecontainer single-wrapper bgw'><div class='authorbox'><div class='row'><div class='col-sm-12 col-md-12'><div class='post clearfix'><div class='avatar-author'><img src='/template/upload/avatar_02.png' class='img-responsive'></div><div class='author-title desc'><div class='author'>Author: " + author + "</div><div class='email'>Email: " + email + "</div><br><div class='text'>" + text + "</div><br><div class='date'>Date: " + date + "</div></div></div></div></div></div>");
                                    $("input#author").val("");
                                    $("input#email").val("");
                                    $("textarea#text").val("");
                                    $("#error-message").remove();
                                }
                            });
                        } else {
                            $('#error-message').html(errors);
                            $('#error-message').show();
                        }
                    });

                });
            </script>
        </div>
    </div>
    <!--end create comment-->

<?php require_once ROOT . '/views/layouts/footer.php' ?>