<?php require_once ROOT . '/views/layouts/header.php' ?>

    <section class="section bgg">
        <div class="container">
            <div class="title-area">
                <div class="col-sm-4 col-sm-offset-4 padding-right">

                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?php echo $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <div class="signup-form"><!--sign up form-->
                        <h2>Sign in</h2>
                        <form action="#" method="post">
                            <p>
                                <input type="email" name="email" placeholder="Email Address"
                                       value="<?php echo $email; ?>"/>
                            </p>
                            <p>
                                <input type="password" name="password" placeholder="Password"
                                       value="<?php echo $password; ?>"/>
                            </p>
                            <p>
                                <input type="submit" name="submit" value="Sign in"/>
                            </p>
                        </form>
                        <a href="?action=register">Create an account</a><br>
                    </div><!--/sign up form-->
                </div>
            </div>
            <!-- /.pull-right -->
        </div>
        <!-- end container -->
    </section>


<?php require_once ROOT . '/views/layouts/footer.php' ?>