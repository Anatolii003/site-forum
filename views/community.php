<?php require_once ROOT . '/views/layouts/header.php' ?>

    <div class="container sitecontainer bgw">
        <div class="row">
            <div class="col-md-12 m22 single-post">
                <div class="widget">
                    <div class="large-widget m30">
                        <div class="post-desc">
                            <div id="bbpress-forums">
                                <div class="table-responsive">
                                    <ul class="bbp-forums">
                                        <li class="bbp-header">
                                            <ul class="forum-titles">
                                                <li class="bbp-forum-info">Topics</li>
                                                <li class="bbp-forum-reply-count">Posts</li>
                                                <?php if ($user['admin'] == 1): ?>
                                                    <li class="bbp-forum-delete-topic">Delete topic</li>
                                                <?php endif; ?>
                                            </ul>
                                        </li><!-- .bbp-header -->

                                        <?php foreach ($topicList as $topic): ?>
                                            <?php $id = $topic['id']; ?>
                                            <?php $total = Comment::getTotalComments($id); ?>
                                            <li class="bbp-body">
                                                <ul class="forum type-forum status-publish hentry loop-item-0 odd bbp-forum-status-open bbp-forum-visibility-publish">
                                                    <li class="bbp-forum-info">
                                                        <a class="bbp-forum-title"
                                                           href="?action=view-topic&id=<?php echo $topic['id'] ?>">
                                                            <?php echo $topic['name'] ?>
                                                        </a>
                                                    </li>
                                                    <li class="bbp-forum-reply-count"><?php echo $total ?></li>
                                                    <?php if ($user['admin'] == 1): ?>
                                                        <button class="bbp-forum-delete-topic"
                                                                id="<?php echo $topic['id'] ?>">Delete
                                                        </button>
                                                    <?php endif; ?>
                                                </ul><!-- end bbp forums -->
                                            </li>
                                        <?php endforeach; ?>
                                    </ul><!-- .forums-directory -->
                                    <div id="new-topic"></div>
                                </div>
                            </div> <!-- /bbpress -->

                        </div>
                        <!-- end post-desc -->
                    </div>
                    <!-- end large-widget -->
                </div>
                <!-- end widget -->

                <!--create topic-->
                    <div class="form">
                        <p>Add topic</p>
                        <p>
                            <textarea name="text" id="text"></textarea>
                        </p>
                        <p>
                            <button id="send">Send</button>
                        </p>
                    </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("button#send").click(function () {
                            var text = $("#text").val();
                            $.ajax({
                                url: "?action=add-topic",
                                type: "POST",
                                data: {text: text},
                                dataType: "json",
                                success: function (result) {
                                    if (result) {
                                        console.log(result);
                                        $('.bbp-body').remove();
                                        $('.bbp-forums').html(result);
                                    }
                                    return false;
                                }

                            });
                        });

                    });
                </script>
                <!--success: function () {
                $("#new-topic").append("<div class='text' style='border: 1px solid #F2F2F2; height: 80px; padding: 25px 5px 5px 20px; font-family: Verdana, Geneva, sans-serif; font-size: 18px'>" + text + "</div>");
                $("textarea#text").val("");
                }-->
                <!--end create topic-->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->


    <!--script delete topic-->
    <script type="text/javascript">
        $(document).ready(function () {
            $('button.bbp-forum-delete-topic').click(function (e) {
                e.preventDefault();
                var parent = $(this).parent();
                var id = $(this).attr("id");
                let isConfirmed = confirm('Are you sure you want to delete this topic?');
                if (isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: '?action=delete-topic',
                        data: {id: id},
                        beforeSend: function () {
                            parent.animate({'backgroundColor': '#fb6c6c'}, 300);
                        },
                        success: function () {
                            parent.slideUp(300, function () {
                                parent.remove();
                            });
                        }
                    });
                }
            });
        });
    </script>
    <!--end script delete topic-->

<?php require_once ROOT . '/views/layouts/footer.php' ?>