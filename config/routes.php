<?php
return array(
    'view-topic' => 'community/topic',
    'add-comment' => 'community/addComment',
    'add-topic' => 'community/addTopic',
    'delete-topic' => 'community/deleteTopic',
    'delete-comment' => 'community/deleteComment',

    'register' => 'user/register',
    'login' => 'user/login',
    'logout' => 'user/logout',

    '' => 'community/index',
);