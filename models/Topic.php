<?php


class Topic
{

    public static function getTopicsList()
    {
        $db = Db::getConnection();

        $topicsList = array();

        $result = $db->query('SELECT id, date, name FROM topics ORDER BY id DESC');

        $i = 0;
        while($row = $result->fetch()){
            $topicsList[$i]['id'] = $row['id'];
            $topicsList[$i]['name'] = $row['name'];
            $topicsList[$i]['date'] = $row['date'];
            $i++;
        }

        return $topicsList;
    }

    public static function getTopicById($id)
    {
        $id = intval($id);
        if($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT name FROM topics WHERE id = ' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            return $result->fetch();
        }
    }

    public static function getTotalTopics($id)
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM topics WHERE category_id = ' . $id);

        $row = $result->fetch();

        return $row['count'];
    }

    public static function addTopic($name, $date)
    {
        $db = Db::getConnection();
        $result = $db->prepare("INSERT INTO topics (name, date) VALUES (:name, :date)");
        $result->bindParam(":name", $name, PDO::PARAM_STR);
        $result->bindParam(":date", $date, PDO::PARAM_STR);
        $result->execute();
        return $result;
    }

    public static function deleteTopic($id)
    {
        $db = Db::getConnection();
        $result = $db->prepare("DELETE FROM topics WHERE id = :id;
                                DELETE FROM comments WHERE topic_id = :id");
        $result->bindParam(":id", $id, PDO::PARAM_INT);
        return $result->execute();
    }

}