<?php

class Router
{

    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }

    private function getURI()
    {
        if(!empty($_GET['action'])){
            return $_GET['action'];
        }
    }

    private function getId()
    {
        if(!empty($_GET['id'])){
            return $_GET['id'];
        }
    }

    public function run()
    {
        /*echo '<pre>';
        print_r($this->routes);
        echo '</pre>';*/

        $uri = $this->getURI();
        $id = $this->getId();

        foreach ($this->routes as $uriPattern => $path) {
            //echo "<br>$uriPattern -> $path<br>";

            if (preg_match("~$uriPattern~", $uri)){

                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $segments = explode('/', $internalRoute);
                array_push($segments, $id);
                /*echo '<pre>';
                var_dump($segments);
                echo '</pre>';*/

                $controllerName = ucfirst(array_shift($segments)).'Controller';
                //echo "<br>Controller Name: $controllerName<br>";

                $actionName = 'action'.ucfirst(array_shift($segments));
                //echo "<br>Action Name: $actionName<br>";

                $parameters = $segments;
                /*echo '<pre>';
                var_dump($parameters);*/


                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile)){
                    include_once ($controllerFile);
                }

                $controllerObject = new $controllerName();
                /*$result = $controllerObject->$actionName($parameters); //Замість цього використовується функція call_user_func_array*/
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                if ($result != null){
                    break;
                }
            }
        }
    }

}

