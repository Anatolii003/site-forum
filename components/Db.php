<?php


class Db
{

    public static function getConnection()
    {
        $arrayParams = ROOT . '/config/db_params.php';
        $params = include $arrayParams;

        $dsn = "mysql:host={$params['host']}; dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);
        return $db;
    }

}