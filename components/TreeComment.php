<?php

class TreeComment
{

    public static function tree($row)
    {
        $user = User::checkLogged();
        echo "<div class=\"container sitecontainer single-wrapper bgw\">
                <div class=\"authorbox\">
                    <div class=\"row\">
                        <div class=\"col-sm-12 col-md-12\">
                            <div class=\"post clearfix\">
                                <div class=\"avatar-author\">
                                    <img alt=\"\" src=\"/template/upload/avatar_02.png\" class=\"img-responsive\">
                                </div>
                                <div class=\"author-title desc\">";
        echo "<div class=\"author\">Author: " . $row['author'] . "</div>";
        echo "<div class=\"email\">Email: " . $row['email'] . "</div><br>";
        echo "<div class=\"text\">" . $row['text'] . "</div><br>";
        echo "<div class=\"date\">Date: " . $row['date'] . "</div>";
        echo "<a href=\"#comment_from\" class=\"reply\" topic_id=\"" . $row['topic_id'] . "\" id=\"" . $row['id'] . "\">Answer</a>";
        echo "</div>";
        if ($user['admin'] == 1) {
            echo "<a href='' class='bbp-forum-delete-topic' id='{$row['id']}'>Delete</a>";
        }
        echo "             </div>
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end authorbox -->
            </div><!-- end container -->";
        echo "<ul>";
        $child = array();
        $child = Comment::getChildComment($row);
        foreach ($child as $res) {
            self::tree($res);
        }
        echo "</ul>";

    }

}