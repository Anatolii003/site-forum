<?php


class CommunityController
{

    public function actionIndex()
    {
        $topicList = array();
        $topicList = Topic::getTopicsList();

        $user = User::checkLogged();

        require_once ROOT . '/views/community.php';
        return true;
    }

    public function actionTopic($id)
    {
        $commentsList = array();
        $commentsList = Comment::getCommentsList($id);

        $topic = Topic::getTopicById($id);

        $topicId = Comment::getTopicId();


        require_once ROOT . '/views/topic.php';
        return true;
    }

    public function actionAddComment()
    {
        $author = $_POST['author'];
        $text = $_POST['text'];
        $parent_id = $_POST['parent_id'];
        $topic_id = $_POST['topic_id'];
        $date = $_POST['date'];
        $email = $_POST['email'];

        Comment::addComment($author, $text, $parent_id, $topic_id, $date, $email);
        return true;
    }

    public function actionAddTopic()
    {
        $text = $_POST['text'];
        $date = date("Y-m-d H:i:s");

        Topic::addTopic($text, $date);

        $user = User::checkLogged();

        $topicList = array();
        $topicList = Topic::getTopicsList();
        /*echo '<br>';
        print_r($topicList);
        echo '<br>';*/

        $result = '';
        foreach($topicList as $item ){
            $button = '';
            if($user['admin'] == 1){
                $button = "<button class='bbp-forum-delete-topic' id='".$item['id']."'>Delete</button>";
            }
            $result .= "<li class='bbp-body'>"
                ."<ul class='forum type-forum status-publish hentry loop-item-0 odd bbp-forum-status-open bbp-forum-visibility-publish'>"
                ."<li class='bbp-forum-info'><a class='bbp-forum-title' href='?action=view-topic&id=".$item['id']."'>".$item['name']."</a></li>".$button."</ul></li>";
        }
        header("Cotent-Type: text/json; charset=utf-8");
        echo json_encode($result);
        return true;
    }

    public function createAddTopic()
    {
        $topic = self::actionAddTopic();
    }


    public function actionDeleteTopic()
    {
        $id = $_POST['id'];
        Topic::deleteTopic($id);
        return true;
    }

    public function actionDeleteComment()
    {
        $id = $_POST['id'];
        $result = array();
        $result = Comment::sele($id);
        Comment::del($id);

        foreach ($result as $r) {
            DeleteComment::delete($r);
        }
        return true;
    }

}