<?php


class UserController
{

    public function actionRegister()
    {

        $name = '';
        $email = '';
        $password = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];


            $errors = array();

            if (User::checkName($name)) {

            } else $errors[] = 'Name must be longer than 3 characters';

            if (User::checkEmail($email)) {

            } else $errors[] = 'Incorrect email';

            if (User::checkPassword($password)) {

            } else $errors[] = 'Password must be longer than 6 characters';

            if (User::checkEmailExist($email)) {
                $errors[] = 'This email already exists';
            }

            if (User::checkNameExist($name)) {
                $errors[] = 'This login already exists';
            }

            if (empty($errors)) {
                $result = User::register($name, $email, $password);
                /*var_dump($result);*/
            }
        }
        require_once ROOT . '/views/user/register.php';
        return true;
    }

    public function actionLogin()
    {
        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = array();

            $user = User::checkUserData($email);


            if ($user == false) {
                $errors[] = 'Wrong email';
            } else {
                if (password_verify($password, $user['password'])) {
                    User::auth($user);
                    header('Location: /');
                } else {
                    $errors[] = 'Wrong password';
                }
            }
        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }

    public function actionLogout()
    {
        unset($_SESSION['user']);
        header("Location: /");
    }

}