<?php

session_start();
ini_set('display errors', 1);
error_reporting(E_ALL);

define('ROOT', dirname(__FILE__));
require_once ROOT . '/components/Router.php';
require_once ROOT . '/components/Autoload.php';

$object = new Router();
$object->run();

/*$action = isset($_GET['action']) ? $_GET['action'] : 'community';
switch($action){
    case 'community':
        $object1 = new CommunityController();
        $object1->actionIndex();
        break;
    case 'view-topic':
        $id = $_GET['id'];
        $object1 = new CommunityController();
        $object1->actionTopic($id);
        break;
    case 'login':
        $object = new UserController();
        $object->actionLogin();
        break;
    case 'register':
        $object = new UserController();
        $object->actionRegister();
        break;
    case 'logout':
        $object = new UserController();
        $object->actionLogout();
        break;
    case 'add-comment':
        $object = new CommunityController();
        $object->actionAddComment();
        break;
    case 'add-topic':
        $object = new CommunityController();
        $object->actionAddTopic();
        break;
    case 'delete-topic':
        $object = new CommunityController();
        $object->actionDeleteTopic();
        break;
    case 'delete-comment':
        $object = new CommunityController();
        $object->actionDeleteComment();
        break;
}


*/
